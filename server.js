const express = require("express");
const http = require("http");
const path = require("path");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const methodOverride = require("method-override");
const assignmentApis = require("./src/modules/modules-routes");
const config = require("./src/config/config");
const RateLimit = require("express-rate-limit");
const applog = require("./src/config/logger").applog;

const request = require("request");
var cron = require("node-cron");

const app = express();
app.use(helmet());
app.use(
  helmet.referrerPolicy({
    policy: "same-origin"
  })
);
app.use(
  helmet.frameguard({
    action: "deny"
  })
);

app.get("/healthcheck", function (req, res) {
  res.json("Server is running!!!");
});
app.set("trust proxy", 1);

app.use(function (req, res, next) {
  if (req.headers["x-api-key"]) {
    jwt.verify(req.headers["x-api-key"], config.secretSalt, function(err, decoded) {
      if (err){
        return res.status(200).json({
          statusCode: "401",
          body: "Unauthorized - XAPI Key"
        });
      }
      next();
    });
  } else {
    console.log("XAPI Key is Missing");
    return res.status(200).json({
      statusCode: "401",
      body: "Unauthorized - XAPI Key"
    });
  }
});

app.set("trust proxy", 1);

var limiter = new RateLimit({
  windowMs: config.rateLimitHr, // 1 minutes
  max: config.rateLimitMax, // limit each IP to 100 requests per windowMs
  message:
    "Too many accounts created from this IP, please try again after an hour"
});
app.use(limiter);

var cors = require("cors");
const wl = config.wia;
const corsOptions = {
  origin: function (origin, callback) {
    if (wl.indexOf(origin) !== -1 || !origin) {
      applog.info("Allowed By Cors:" + origin);
      callback(null, true);
    } else {
      applog.info("Not Allowed By Cors:" + origin);
      callback(new Error("Not allowed by CORS"));
    }
  }
};
app.options("*", cors(corsOptions));
app.use(cors(corsOptions));

app.set("port", process.env.PORT || 8080);
app.use(bodyParser.json({ limit: '50mb' }));
app.use(
  bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000,
  })
);
app.use(methodOverride());
app.use(express.static(path.join(__dirname, "public")));
app.use(function (req, res, next) {
  global.__basedir = __dirname;
  next();
});

assignmentApis(app);

var server = http.createServer(app).listen(app.get("port"), function () {
  console.log("Express server listening on port " + app.get("port"));
});
server.timeout = config.serverTimout;
