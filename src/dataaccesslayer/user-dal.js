const _ = require("lodash");
const helper = require("../modules/common/helper");
const {
  user
} = require("../modules/common/sequelize");
const splunk = require("../modules/common/splunk-logging").splunk;

/**
 * Function to check user is exist or not from user table
 */
module.exports.getUserDetails = function (username,password) {
  return new Promise((resolve, reject) => {
    let query = {};
    query.limit = 1;
    query.where = {
      email: username,
      password: helper.encryptData(password),
      isDelete: 0
    };
    query.attributes = ['firstname'];
    user.findAll(query)
      .then(userDataObj => {
        splunk.success("Successfully retrieved user data....");
        resolve(userDataObj);
      })
      .catch(err => {
        splunk.failure("Error....", err);
        reject(err);
      });
  });
};
/*******END*******/

