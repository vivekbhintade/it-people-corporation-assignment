const _ = require("lodash");
const {
  todo
} = require("../modules/common/sequelize");
const splunk = require("../modules/common/splunk-logging").splunk;

/**
 * Function to create todo
 */
module.exports.create = function (data) {
  return new Promise((resolve, reject) => {
    data["createdBy"] = "SYSTEM";
    data["createdAtdate"] = new Date();
    todo.create(
      data
    )
      .then(dataDtls => {
        splunk.success("Todo created successfully.");
        resolve({
          statusCode: 200,
          message: "Successfully Created"
        });
      })
      .catch(err => {
        splunk.failure("Create todo failed.");
        reject(err);
      });
  });
};
/*******END*******/

/**
 * Function to update todo
 */
module.exports.update = function (data) {
  return new Promise((resolve, reject) => {
    data["updatedBy"] = "SYSTEM";
    data["updatedAtdate"] = new Date();
    todo.update(
      data,
      {
        id : data.id
      }
    )
      .then(dataDtls => {
        splunk.success("Todo updated successfully.");
        resolve({
          statusCode: 200,
          message: "Successfully Updated"
        });
      })
      .catch(err => {
        splunk.failure("Update todo failed.");
        reject(err);
      });
  });
};
/*******END*******/

/**
 * Function to delete todo
 */
module.exports.delete = function (id) {
  return new Promise((resolve, reject) => {
    todo.delete(
      {
        id : data.id
      }
    )
      .then(dataDtls => {
        splunk.success("Todo deleted successfully.");
        resolve({
          statusCode: 200,
          message: "Successfully Deleted"
        });
      })
      .catch(err => {
        splunk.failure("Deleted todo failed.");
        reject(err);
      });
  });
};
/*******END*******/

/**
 * Function to list todo
 */
module.exports.getList = function () {
  return new Promise((resolve, reject) => {
    todo.findAll()
      .then(data => {
        splunk.success("List Todo successfully.");
        resolve({
          statusCode: 200,
          data: data
        });
      })
      .catch(err => {
        splunk.failure("List todo failed.");
        reject(err);
      });
  });
};
/*******END*******/

