module.exports = (sequelize, type) => {
  return sequelize.define(
    "user",
    {
      id: {
        type: type.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      email: {
        type: type.STRING(50),
        allowNull: false
      },
      firstname: {
        type: type.STRING(255),
        allowNull: false
      },
      lastname: {
        type: type.STRING(255),
        allowNull: false
      },
      password: {
        type: type.STRING(255),
        allowNull: false
      },
      isDelete: {
        type: type.INTEGER,
        allowNull: false
      },
      created_by: {
        type: type.STRING,
        allowNull: true,
      },
      updated_by: {
        type: type.STRING,
        allowNull: true,
      },
      createdAt: {
        type: type.DATE,
        allowNull: true,
        defaultValue: type.NOW
      },
      updatedAt: {
        type: type.DATE,
        allowNull: true,
        defaultValue: type.NOW
      }
    },
    {
      freezeTableName: true,
      tableName: "user"
    }
  );
};
