module.exports = (sequelize, type) => {
    return sequelize.define('todo', {
        id: {
            type: type.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        title: {
            type: type.STRING(50),
            allowNull: false
        },
        description: {
            type: type.STRING(255),
            allowNull: false
        },
        start_date: {
            type: type.DATE,
            allowNull: false
        },
        end_date: {
            type: type.DATE,
            allowNull: false
        },
        assignee: {
            type: type.INTEGER,
            allowNull: false
        },
        created_by: {
            type: type.STRING,
            allowNull: true,
        },
        updated_by: {
            type: type.STRING,
            allowNull: true,
        },
        createdAt: {
            type: type.DATE,
            allowNull: true,
            defaultValue: type.NOW
        },
        updatedAt: {
            type: type.DATE,
            allowNull: true,
            defaultValue: type.NOW
        }
    }, {
        freezeTableName: true,
        tableName: 'todo'
    });
};