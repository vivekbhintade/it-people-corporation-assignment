/**
 * Configurations of logger.
 */
const winston = require('winston');
const winstonRotator = require('winston-daily-rotate-file');
var dotenv = require('dotenv').config();

const {
  createLogger,
  format,
  transports
} = require('winston');
const {
  combine,
  timestamp,
  label,
  prettyPrint
} = format;

var transport = new(winston.transports.DailyRotateFile)({
  filename: './logs/app-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '3d'
});
transport.on('rotate', function (oldFilename, newFilename) {
  // do something fun
});

winston.format.combine(
  winston.format.colorize(),
  winston.format.json()
);

let alignColorsAndTime = winston.format.combine(
  winston.format.colorize({
    all: true
  }),
  winston.format.label({
    label: '[LOGGER]'
  }),
  winston.format.timestamp({
    format: "YYYY-MM-DD HH:MM:SS"
  }),
  winston.format.printf(
    info => `${info.timestamp}  ${info.level} : ${info.message}`
  )
);

var logger = winston.createLogger({
  level: 'info',
  format: combine(
    timestamp(),
    prettyPrint()
  ),
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(winston.format.colorize(), alignColorsAndTime)
    }),
    transport
  ],
  exitOnError: false
});


module.exports = {
  'applog': logger,
};