for (let key in console) {
    if (console.hasOwnProperty(key)) {
      module.exports[key] = console[key];
    }
  }
  module.exports.success = function () {
    const args = Array.from(arguments);
    args.unshift('success -');
    return console.log.apply(console, args);
  };
  
  module.exports.info = function () {
    const args = Array.from(arguments);
    args.unshift('info -');
    return console.log.apply(console, args);
  };
  
  module.exports.error = function () {
    const args = Array.from(arguments);
    args.unshift('error -');
    return console.error.apply(console, args);
  };
  
  module.exports.fatal = function () {
    const args = Array.from(arguments);
    args.unshift('fatal -');
    return console.error.apply(console, args);
  };
  
  module.exports.warn = function () {
    const args = Array.from(arguments);
    args.unshift('warn -');
    return console.warn.apply(console, args);
  };
  module.exports.log = function () {
    const args = Array.from(arguments);
    args.unshift('log -');
    return console.log.apply(console, args);
  };
  
  process.on('uncaughtException', err => {
    module.exports.fatal('Caught exception', err);
  });