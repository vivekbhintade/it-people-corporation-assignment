var _ = require("lodash");
const cdpojb = require("../modules/common/cdp");
let cdpdtls = cdpojb.init();
var common = {};
if (cdpdtls && cdpdtls.mssql) {
  common = {
    dbConfig: {
      dbName:
        typeof global.fromunittest === "undefined" || !global.fromunittest
          ? cdpdtls.mssql.database
          : null,
      dbUserName:
        typeof global.fromunittest === "undefined" || !global.fromunittest
          ? cdpdtls.mssql.user
          : null,
      dbPassword:
        typeof global.fromunittest === "undefined" || !global.fromunittest
          ? cdpdtls.mssql.password
          : null,
      dbHost:
        typeof global.fromunittest === "undefined" || !global.fromunittest
          ? cdpdtls.mssql.server
          : null,
      dbPort:
        typeof global.fromunittest === "undefined" || !global.fromunittest
          ? cdpdtls.mssql.port
          : null
    },
    sequelizeLogging: process.env.sequelizeLogging || true,
    wia: (typeof(process.env.wia) === 'undefined' || typeof(process.env.wia) === null || typeof(process.env.wia) === '') ? '' : process.env.wia.split(','),
    encryptionKey: process.env.SECRET_ENCRYPTIONKEY,
    secretSalt: process.env.SECRET_SALT,
    rateLimitHr: process.env.rateLimitHr,
    rateLimitMax: process.env.rateLimitMax
  };
}

module.exports = common;
