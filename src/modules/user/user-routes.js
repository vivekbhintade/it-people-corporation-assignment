const express = require("express");
const userController = require("./user-controller");
let expressRouter = express.Router();
let UserController = new userController();
module.exports = function calc(app) {

  expressRouter
    .route("/login")
    .post(UserController.login.bind(UserController));

  return expressRouter;
};
