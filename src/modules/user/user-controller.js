const _ = require("lodash");
const { validateAll, sanitize } = require("indicative");
const config = require("../../config/config");
const validationRules = require("../../validation-rules/rules");
const splunk = require("../common/splunk-logging").splunk;

let me = {};
module.exports = class UserController {
  constructor() {
    me = this;
  }

  async login(req, res) {
    try {
      let requestParameter = JSON.parse(JSON.stringify(req.body));
      try {
        await validateAll(
          requestParameter,
          validationRules.loginRule,
          validationRules.validationMessage
        );
        let newdataObj = await sanitize(
          requestParameter,
          validationRules.loginRuleSanitize
        );
        if (!_.isEqual(requestParameter, newdataObj)) {
          throw new Error("Bad Request");
        }
      } catch (e) {
        e = e[0] ? e : e.toString();
        return res.status(200).json({
          statusCode: 400,
          body: e
        });
      }
      Promise.all([
        userDAL.getUserDetails(requestParameter)
      ])
      .then(userDataObj => {
          var userData = JSON.parse(JSON.stringify(userDataObj));
          if(userData){
            var userToken = jwt.sign(userData, config.secretSalt, {
              expiresIn: 86400
            });
            return res.json({
              status: 200,
              userToken: userToken
            });          
          }
          else{
            return res.json({
              statusCode: 404,
              body: "User not found...!!!"
            });
          }
          
        })
        .catch(error => {
          splunk.failure("Quote Api Error", error.toString());
          return res.json({
            statusCode: 400,
            body: error.toString()
          });
        });
    } catch (err) {
      splunk.failure("Quote Api Error", err.toString());
      return res.json({
        statusCode: 400,
        body: err.toString()
      });
    }
  }
};
