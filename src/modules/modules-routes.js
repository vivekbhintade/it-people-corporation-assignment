const express = require('express');
let expressRouter = express.Router();
const todo = require('./todo/todo-routes');
const user = require('./user/user-routes');

module.exports = function assignmentApis(app) {
	app.use('/todo', todo(app));
	app.use('/user', user(app));
	return app;
};

