const Sequelize = require("sequelize");
const config = require("../../config/config");
const dbConfig = config.dbConfig;
const TodoModel = require("../../models/todo");
const UserModel = require("../../models/user");

const sequelize = new Sequelize(
  dbConfig.dbName,
  dbConfig.dbUserName,
  dbConfig.dbPassword,
  {
    host: dbConfig.dbHost,
    dialect: "mssql",
    dialectOptions: {
      options: {
        encrypt: true,
        supportBigNumbers: true,
        bigNumberStrings: true,
        connectionTimeout: 300000,
        requestTimeout: 300000,
        timeout: 100000
      }
    },
    logging: false,
    pool: {
      max: 50,
      min: 0,
      idle: 100000,
      acquire: 1000000
    }
  }
);

let todo = TodoModel(sequelize, Sequelize);
let user = UserModel(sequelize, Sequelize);

module.exports = {
  todo,
  user,
  sequelize
};
