const config = require('../../config/config');
const applog = require('../../config/logger').applog;

module.exports.splunk = {
    success: function (msg, successObj, oPar) {
        if (successObj) {
            applog.info('Success: ' + msg + JSON.stringify(successObj))
        } else {
            applog.info('Success: ' + msg)
        }
    },
    info: function (msg, infoObj, oPar) {
        if (infoObj) {
            applog.info('Info: ' + msg + JSON.stringify(infoObj))
        } else {
            applog.info('Info: ' + msg)
        }
    },
    failure: function (msg, errorObj, oPar) {
        if (errorObj) {
            applog.error('Error : ' + msg + JSON.stringify(errorObj))
        } else {
            applog.error('Error: ' + msg)
        }
    }
}