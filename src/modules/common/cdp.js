require('dotenv').config();

const getMSSQL = () => {
	const mssql = {
		min: Number(process.env.MSSQL_POOL_MIN) || 1,
		max: Number(process.env.MSSQL_POOL_MAX) || 10,
		timeout: Number(process.env.MSSQL_TIMEOUT) || 3000,
		timezone: 'UTC',
		server: process.env.MSSQL_HOST,
		port: Number(process.env.MSSQL_PORT),
		user: process.env.MSSQL_USER,
		password: process.env.MSSQL_PASSWORD,
		database: process.env.MSSQL_DATABASE,
		options: { encrypt: !!Number(process.env.MSSQL_ENCRYPT) }
	};
	return mssql;
};

module.exports.init = function() {
	return {
		env: process.env.NODE_ENV || 'production',
		http: {
			host: process.env.SERVER_HOST || 'localhost',
			port: Number(process.env.SERVER_PORT) || 8080,
			gracefulTimeout: Number(process.env.SERVER_GRACEFUL_TIMEOUT) || 10000,
		},
		mssql: getMSSQL()
	};
};

