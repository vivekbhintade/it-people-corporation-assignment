const CryptoJS = require("crypto-js");
const config = require("../../config/config");
const applog = require("../../config/logger").applog;
const uuidv4 = require("uuid/v4");
module.exports.guid = function () {
  return uuidv4();
};

module.exports.encryptData = function (data) {
  let encryptedValue = "";
  let encryptedValue = CryptoJS.AES.encrypt(
        data,
        config.encryptionKey
      );
  return encryptedValue.toString();
};

module.exports.decryptData = function (encryptedData) {
  try {
    let decryptedValue = "";
      let bytes = CryptoJS.AES.decrypt(
          val.toString(),
          encryptionConfig.encryptionKey
        );
        decryptedValue = bytes.toString(CryptoJS.enc.Utf8);
      return decryptedValue.toString();
  } catch (e) {
    applog.error(e);
  }
  return encryptedData;
};

