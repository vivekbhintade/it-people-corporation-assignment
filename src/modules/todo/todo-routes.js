const express = require("express");
const todoController = require("./todo-controller");
let expressRouter = express.Router();
let TodoController = new todoController();
module.exports = function calc(app) {

  expressRouter
    .route("/list")
    .get(TodoController.list.bind(TodoController));

  expressRouter
    .route("/create")
    .post(TodoController.createTodo.bind(TodoController));

  expressRouter
    .route("/update")
    .post(TodoController.updateTodo.bind(TodoController));

  expressRouter
    .route("/delete")
    .post(TodoController.deleteTodo.bind(TodoController));

  return expressRouter;
};
