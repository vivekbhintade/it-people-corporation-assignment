const _ = require("lodash");
const { validateAll, sanitize } = require("indicative");
const validationRules = require("../../validation-rules/rules");
const splunk = require("../common/splunk-logging").splunk;
const todoDAL = require("../../dataaccesslayer/todo-dal");

let me = {};
module.exports = class TodoController {
  constructor() {
    me = this;
  }

  async createTodo(req, res) {
    try {
      let requestParameter = JSON.parse(JSON.stringify(req.body));
      console.log(requestParameter);
      try {
        await validateAll(
          requestParameter,
          validationRules.createTodoRule,
          validationRules.validationMessage
        );
        let newdataObj = await sanitize(
          requestParameter,
          validationRules.createTodoRuleSanitize
        );
        if (!_.isEqual(requestParameter, newdataObj)) {
          throw new Error("Bad Request");
        }
      } catch (e) {
        e = e[0] ? e : e.toString();
        return res.status(200).json({
          statusCode: 400,
          body: e
        });
      }
      Promise.all([
        todoDAL.create(requestParameter)
      ])
      .then(todoDataObj => {
          var todoData = JSON.parse(JSON.stringify(todoDataObj));
          if(todoData){
            return res.json({
              status: 200,
              data: todoData
            });          
          }
          else{
            return res.json({
              statusCode: 404,
              message: "Failed to create todo...!!!"
            });
          }
        })
        .catch(error => {
          splunk.failure("Create todo Api Error", error.toString());
          return res.json({
            statusCode: 400,
            body: error.toString()
          });
        });
    } catch (err) {
      splunk.failure("Create todo Api Error", err.toString());
      return res.json({
        statusCode: 400,
        body: err.toString()
      });
    }
  }

  async updateTodo(req, res) {
    try {
      let requestParameter = JSON.parse(JSON.stringify(req.body));
      try {
        await validateAll(
          requestParameter,
          validationRules.updateTodoRule,
          validationRules.validationMessage
        );
        let newdataObj = await sanitize(
          requestParameter,
          validationRules.updateTodoRuleSanitize
        );
        if (!_.isEqual(requestParameter, newdataObj)) {
          throw new Error("Bad Request");
        }
      } catch (e) {
        e = e[0] ? e : e.toString();
        return res.status(200).json({
          statusCode: 400,
          body: e
        });
      }
      Promise.all([
        todoDAL.update(requestParameter)
      ])
      .then(todoDataObj => {
          var todoData = JSON.parse(JSON.stringify(todoDataObj));
          if(todoData){
            return res.json({
              status: 200,
              message: todoData.message
            });          
          }
          else{
            return res.json({
              statusCode: 404,
              message: "Failed to update todo...!!!"
            });
          }
        })
        .catch(error => {
          splunk.failure("Update todo Api Error", error.toString());
          return res.json({
            statusCode: 400,
            body: error.toString()
          });
        });
    } catch (err) {
      splunk.failure("Update todo Api Error", err.toString());
      return res.json({
        statusCode: 400,
        body: err.toString()
      });
    }
  }

  async deleteTodo(req, res) {
    try {
      let requestParameter = JSON.parse(JSON.stringify(req.body));
      try {
        await validateAll(
          requestParameter,
          validationRules.deleteTodoRule,
          validationRules.validationMessage
        );
        let newdataObj = await sanitize(
          requestParameter,
          validationRules.deleteTodoRuleSanitize
        );
        if (!_.isEqual(requestParameter, newdataObj)) {
          throw new Error("Bad Request");
        }
      } catch (e) {
        e = e[0] ? e : e.toString();
        return res.status(200).json({
          statusCode: 400,
          body: e
        });
      }
      Promise.all([
        todoDAL.delete(requestParameter.id)
      ])
      .then(todoDataObj => {
          var todoData = JSON.parse(JSON.stringify(todoDataObj));
          if(todoData){
            return res.json({
              status: 200,
              message: todoData.message
            });          
          }
          else{
            return res.json({
              statusCode: 404,
              message: "Failed to delete todo...!!!"
            });
          }
        })
        .catch(error => {
          splunk.failure("Delete todo Api Error", error.toString());
          return res.json({
            statusCode: 400,
            body: error.toString()
          });
        });
    } catch (err) {
      splunk.failure("Delete todo Api Error", err.toString());
      return res.json({
        statusCode: 400,
        body: err.toString()
      });
    }
  }

  async list(req, res) {
    try {
      Promise.all([
        todoDAL.getList()
      ])
      .then(todoDataListObj => {
          var todoData = JSON.parse(JSON.stringify(todoDataListObj));
          if(todoData){
            return res.json({
              status: 200,
              data: todoData
            });          
          }
          else{
            return res.json({
              statusCode: 404,
              message: "Failed to create todo...!!!"
            });
          }
        })
        .catch(error => {
          splunk.failure("List todo Api Error", error.toString());
          return res.json({
            statusCode: 400,
            body: error.toString()
          });
        });
    } catch (err) {
      splunk.failure("List todo Api Error", err.toString());
      return res.json({
        statusCode: 400,
        body: err.toString()
      });
    }
  }
};
