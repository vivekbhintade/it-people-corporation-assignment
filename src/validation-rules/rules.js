module.exports.createTodoRule = {
  title: "required|string",
  description: "required|string",
  start_date: "required|string",
  end_date: "required|string",
  assignee : "required|number"
};

module.exports.createTodoRuleSanitize = {
  title: "trim|strip_tags|escape|strip_links",
  description: "trim|strip_tags|escape|strip_links",
  start_date: "trim|strip_tags|escape|strip_links",
  end_date: "trim|strip_tags|escape|strip_links",
  assignee: "trim|strip_tags|escape|strip_links"
};

module.exports.updateTodoRule = {
  title: "required|string",
  description: "required|string",
  start_date: "required|string",
  end_date: "required|string",
  assignee : "required|number",
  id : "required|number"
};

module.exports.updateTodoRuleSanitize = {
  title: "trim|strip_tags|escape|strip_links",
  description: "trim|strip_tags|escape|strip_links",
  start_date: "trim|strip_tags|escape|strip_links",
  end_date: "trim|strip_tags|escape|strip_links",
  assignee: "trim|strip_tags|escape|strip_links",
  id: "trim|strip_tags|escape|strip_links"
};

module.exports.deleteTodoRule = {
  id : "required|number"
};

module.exports.updateTodoRuleSanitize = {
  id: "trim|strip_tags|escape|strip_links"
};

module.exports.loginRule = {
  username: "required|string",
  password: "required|string"
};

module.exports.loginRuleSanitize = {
  username: "trim|strip_tags|escape|strip_links",
  password: "trim|strip_tags|escape|strip_links"
};

module.exports.validationMessage = {
  required: "The field is required.",
  string: "The field must be of string type.",
  alpha: "The field must be alphabetical string.",
  integer: "The field must be of integer type.",
  number: "The field must be of number type."
};
