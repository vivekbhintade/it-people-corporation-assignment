#Instructions to use this repository

Install all dependencies, compile and run tests:

Please use node version 10.15.3 for all test cases to get execute by following command
nvm use 10.15.3
npm install

Database script is present in following folder. you can execute this script to create database schema.

"environments/sql/dev/tables/application.sql"

Run the main program

"npm run start"

Run the test cases

"npm run test"

Problem Statement:
-----------------------
Pointy-haired-boss wants to be more organized.  So, he wants a web application where he can
login and get all the TODOs he has created.  Pointy-haired-boss also wants to be able to
update and delete the TODOs.
 
The frontend will be built by someone else.
 
Technical Requirement
Please create the following:
- REST APIs
- API to authenticate
- APIs for TODO CRUD
- Protect the API using JWT
- Persist TODOs
- Must have unit test cases