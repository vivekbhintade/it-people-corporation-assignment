IF (NOT EXISTS (SELECT * FROM sys.DATABASE WHERE name = 'application')) 
BEGIN
    EXEC ('CREATE DATABASE application')
END
GO

DROP TABLE IF EXISTS `application-dev`.todo;  
GO 

CREATE TABLE `application-dev`.todo (
	id INT auto_increment NOT NULL PRIMARY KEY,
	title VARCHAR(50) NOT NULL,
	description varchar(255) NOT NULL,
	start_date DATETIME NOT NULL,
	end_date DATETIME NOT NULL,
	created_by varchar(255) DEFAULT 'SYSTEM' NOT NULL,
	updated_by varchar(255) DEFAULT 'SYSTEM' NULL,
	createdAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
	updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NULL
)

GO

DROP TABLE IF EXISTS `application-dev`.user;  
GO 

CREATE TABLE `application-dev`.user (
	id INT auto_increment NOT NULL PRIMARY KEY,
	email VARCHAR(50) NOT NULL,
	firstname varchar(255) NOT NULL,
	lastname varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	isDelete varchar(255) NOT NULL,
	assignee INT NOT NULL,
	created_by varchar(255) DEFAULT 'SYSTEM' NOT NULL,
	updated_by varchar(255) DEFAULT 'SYSTEM' NULL,
	createdAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
	updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NULL
)

GO

ALTER TABLE `application-dev`.todo ADD assignee INT NULL;
GO