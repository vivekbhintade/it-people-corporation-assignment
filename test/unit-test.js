global.fromunittest = true;
let chai = require("chai");
let expect = require("chai").expect;
let should = require("chai").should;

chai.use(require("chai-like"));
chai.use(require("chai-things"));

//Mock Library
const SequelizeMock = require("sequelize-mock");
//Mock database connection
const DBConnectionMock = new SequelizeMock();
let proxyquire = require("proxyquire");
let sequelizeMockConnection = new SequelizeMock(
    "dummyDatabase",
    "dummyUsername",
    "dummyPassword",
    {
        dialect: "mock"
    }
);

//Mock Models
let UserMock = DBConnectionMock.define("user", {
    id: "282ae0c3-9ca0-4594-9a31-0c82e47192b4",
    email: "test@gmail.com",
    firstname: "Test",
    lastname: "Test",
    password: "6548d382-07db-4613-9d00-2efd3aac9d",
    isDelete: 0,
    created_by: "6548d382-07db-4613-9d00-2efd3aac9d08",
    updated_by: "6548d382-07db-4613-9d00-2efd3aac9d08"
});

let TodoMock = DBConnectionMock.define("todo", {
    id: 'a330ce4a-1e61-454d-8f1f-0ec2cb08a4a0',
    title: 'Test Title',
    description:'test Description',
    start_date: '2021-01-04',
    end_date: '2021-01-04',
    assignee: 'a330ce4a-1e61-454d-8f1f-0ec2cb08a4a0',
    created_by: "6548d382-07db-4613-9d00-2efd3aac9d08",
    updated_by: "6548d382-07db-4613-9d00-2efd3aac9d08"
})

//Mock Error Model
let UserMockError = DBConnectionMock.define("user", null);
let TodoMockError = DBConnectionMock.define(
    "todo",
    null
);

const userDal = proxyquire(
    "../src/dataaccesslayer/user-dal.js",
    {
        "../modules/common/sequelize": {
            user: UserMock
        }
    }
);

const todoDal = proxyquire(
    "../src/dataaccesslayer/todo-dal.js",
    {
        "../modules/common/sequelize": {
            todo: TodoMock
        }
    }
);

const userDalError = proxyquire(
    "../src/dataaccesslayer/user-dal.js",
    {
        "../modules/common/sequelize": {
            UserError: UserMockError
        }
    }
);

const todoDalError = proxyquire(
    "../src/dataaccesslayer/todo-dal.js",
    {
        "../modules/common/sequelize": {
            TodoError: TodoMockError
        }
    }
);

describe("#todoDal.createTodo-positive", () => {
    it("should return success from createTodo", done => {
        let todoData = [
            {
                title: 'Test Title',
                description:'test Description',
                start_date: '2021-01-04',
                end_date: '2021-01-04',
                assignee: 'a330ce4a-1e61-454d-8f1f-0ec2cb08a4a0',
                created_by: "6548d382-07db-4613-9d00-2efd3aac9d08",
                updated_by: "6548d382-07db-4613-9d00-2efd3aac9d08"
            }
        ];
        todoDal
            .create(todoData)
            .then(result => {
                expect(result).to.equal({
                    message: "Successfully Created"
                });
                done();
            })
            .catch(catcherr => {
                done();
            });
    });
});

describe("#todoDalError.createTodo-negative", () => {
    it("should return error from createTodo", done => {
        let todoData = [
            {
                title: 'Test Title',
                description:'test Description',
                start_date: '2021-01-04',
                end_date: '2021-01-04',
                assignee: 'a330ce4a-1e61-454d-8f1f-0ec2cb08a4a0',
                created_by: "6548d382-07db-4613-9d00-2efd3aac9d08",
                updated_by: "6548d382-07db-4613-9d00-2efd3aac9d08"
            }
        ];
        todoDalError
            .create(todoData)
            .then(result => { })
            .catch(catcherr => {
                expect(catcherr.toString());
                done();
            });
    });
});

describe("#todoDal.updateTodo-positive", () => {
    it("should return success from updateTodo", done => {
        let todoData = [
            {
                id: 'a330ce4a-1e61-454d-8f1f-0ec2cb08a4a0',
                title: 'Test Title',
                description:'test Description',
                start_date: '2021-01-04',
                end_date: '2021-01-04',
                assignee: 'a330ce4a-1e61-454d-8f1f-0ec2cb08a4a0',
                updated_by: "6548d382-07db-4613-9d00-2efd3aac9d08"
            }
        ];
        todoDal
            .update(todoData)
            .then(result => {
                expect(result).to.equal({
                    message: "Successfully Updated"
                });
                done();
            })
            .catch(catcherr => {
                done();
            });
    });
});

describe("#todoDalError.updateTodo-negative", () => {
    it("should return error from updateTodo", done => {
        let todoData = [
            {
                id: 'a330ce4a-1e61-454d-8f1f-0ec2cb08a4a0',
                title: 'Test Title',
                description:'test Description',
                start_date: '2021-01-04',
                end_date: '2021-01-04',
                assignee: 'a330ce4a-1e61-454d-8f1f-0ec2cb08a4a0',
                created_by: "6548d382-07db-4613-9d00-2efd3aac9d08",
                updated_by: "6548d382-07db-4613-9d00-2efd3aac9d08"
            }
        ];
        todoDalError
            .update(todoData)
            .then(result => { })
            .catch(catcherr => {
                expect(catcherr.toString());
                done();
            });
    });
});

describe("#todoDal.deleteTodo-positive", () => {
    it("should return success from deleteTodo", done => {
        todoDal
            .delete({id : 'a330ce4a-1e61-454d-8f1f-0ec2cb08a4a0'})
            .then(result => {
                expect(result).to.equal("success");
                done();
            })
            .catch(catcherr => {
                done();
            });
    });
});

describe("#todoDal.deleteTodo-negative", () => {
    it("should return error from deleteTodo", done => {
        todoDalError
            .delete({id : 'a330ce4a-1e61-454d-8f1f-0ec2cb08a4a0'})
            .then(result => { })
            .catch(catcherr => {
                expect(catcherr.toString());
                done();
            });
    });
});

describe("#todoDal.listTodo-positive", () => {
    it("should return success from listTodo", done => {
        todoDal
            .getList()
            .then(result => {
                console.log(result);
                expect(result).to.equal({
                    statusCode: 200
                });
                done();
            })
            .catch(catcherr => {
                done();
            });
    });
});

describe("#todoDal.listTodo-negative", () => {
    it("should return error from listTodo", done => {
        todoDalError
            .getList()
            .then(result => { })
            .catch(catcherr => {
                expect(catcherr.toString());
                done();
            });
    });
});

describe("#userDal.login-positive", () => {
    it("should return success from login", done => {
        userDal
            .getUserDetails('test@gmail.com','6548d382-07db-4613-9d00-2efd3aac9d')
            .then(result => {
                expect(result).to.equal("success");
                done();
            })
            .catch(catcherr => {
                done();
            });
    });
});

describe("#userDal.login-negative", () => {
    it("should return error from login", done => {
        userDalError
            .getUserDetails('test@gmail.com','6548d382-07db-4613-9d00-2efd3aac9d')
            .then(result => { })
            .catch(catcherr => {
                expect(catcherr.toString());
                done();
            });
    });
});